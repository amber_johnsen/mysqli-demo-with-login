SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- Create Database if not hosted on azure

-- CREATE DATABASE IF NOT EXISTS `jk11_mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
--USE `jk11_mydb`;

-- --------------------------------------------------------

-- Table structure for table `tbl_people`
DROP TABLE IF EXISTS `tbl_people`;
CREATE TABLE `tbl_people` (
  `ID` int(11) NOT NULL,
  `FNAME` varchar(30) NOT NULL,
  `LNAME` varchar(50) NOT NULL,
  `DOB` date NOT NULL,
  PRIMARY KEY ( `ID` )
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- Table structure for table `tbl_users`
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `USERNAME` varchar(20) NOT NULL,
  `PASSWRD` varchar(30) NOT NULL,
  PRIMARY KEY ( `ID` )
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Clear data for table `tbl_people`
-- TRUNCATE TABLE `tbl_people`
-- INSERT INTO `tbl_people` (`ID`, `FNAME`, `LNAME`, `DOB`) VALUES
-- (31, 'Jeffrey', 'Kranenburg', '1982-10-23'),
-- (32, 'Hello', 'World!', '1982-01-01'),
-- (33, 'Lisa', 'Simpson', '1985-11-23');

-- Clear data for table `tbl_users`
-- TRUNCATE TABLE `tbl_users`
-- INSERT INTO `tbl_users` (`ID`, `USERNAME`, `PASSWRD`) VALUES (1, 'Admin', 'admin123');