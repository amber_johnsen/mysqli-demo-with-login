<?php include_once('../functions/functions.php'); ?> 
<?php 
    session_start();
    removeSingleRecord();
?>

<!doctype html>
<html>
    <head>
        <title>People</title>
        <link rel="stylesheet" href="../css/main.css" type="text/css">
    </head>
    <body>
        <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>
        <div class="container">
            <h1>People</h1>
            <h2 class="left">Deleteing a record</h2>
            <header class="headerRemove">
                <span class="removeSure">Are you sure you want to remove this record?</span>
            </header>
            <form method='POST' >
                <input type="hidden" name="id" value="<?php echo displayID(); ?>">
                <input type="submit" name="removeRecord" value="submit form">
            </form>
            <h2 class="left"><a href="index.php">Cancel</a></h2>
        </div>
        <?php  
        }
        else
        {
        ?>
        <div class="container">
            <h1>People</h1>
            <h1 class="removeSure">You do not have access to this page</h1>
            <h2><a href="../login.php">Go to the login screen</a></h2>
            <h2><a href="../index.php">Go back to the home screen</a></h2>
        </div>
        <?php
        }
        ?>
    </body>
</html>   